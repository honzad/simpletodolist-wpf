﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

using SimpleTodoList.Commands;
using SimpleTodoList.Controllers;


namespace SimpleTodoList
{
    class MainWindowViewModel: INotifyPropertyChanged
    {
        private readonly TodoHandler _todoHandler;   

        public TodoDoneCommand OnTodoDoneCommand { get; private set; }

        public ButtonPressedCommand OnButtonPressedCommand { get; private set; }

        public ObservableCollection<TodoItem> TodoItems
        {
            get
            {
                if (SearchMode)
                {
                    return startSearch();
                } else {
                    return _todoHandler.Todos;
                }
            }
        }

        private bool _searchMode = false;
        public bool SearchMode
        {
            get { return _searchMode; }
            set
            {
                _searchMode = value;
                OnPropertyChanged("SearchMode");
                OnPropertyChanged("TodoItems");
            }
        }

        private string _inputText = "";
        public string InputText
        {
            get { return _inputText; }
            set
            {
                _inputText = value;
                OnPropertyChanged("InputText");
                OnPropertyChanged("SearchMode");
                OnPropertyChanged("TodoItems");
            }
        }

        public MainWindowViewModel()
        {
            OnTodoDoneCommand = new TodoDoneCommand(onTodoDoneCommand);
            OnButtonPressedCommand = new ButtonPressedCommand(onButtonPressed);

            _todoHandler = new TodoHandler();
        }

        private void onTodoDoneCommand(string id)
        {
            _todoHandler.Remove(id);
        }

        private void onButtonPressed(string buttonType)
        {
            switch (buttonType)
            {
                case "search":
                    setSearchMode();
                    break;

                case "newTodo":
                    createNewTodoCard();
                    break;
            }
        }

        private void createNewTodoCard()
        {
            if (InputText != "")
            {
                int priority = 0;
                string finalText = "";
                if (InputText.IndexOf("!") > -1)
                {
                    string[] arr = InputText.Substring(InputText.IndexOf("!")).Substring(1).Split(' ');
                    string firstWord = InputText.Substring(InputText.IndexOf("!")).Substring(1).Split(' ')[0];
                    if(firstWord != " ")
                    {
                        int.TryParse(firstWord, out priority);
                    }

                    if (priority < 0) { priority = 0; }
                    if (priority > 3) { priority = 3; }

                    string pre = InputText.Substring(0, InputText.IndexOf("!") - 1);
                    string[] after = arr.Where(w => w != firstWord).ToArray();

                    finalText = pre + " " + String.Join(" ", after);
                } else
                {
                    finalText = InputText;
                }
                _todoHandler.Add(new TodoItem(finalText, priority));
                InputText = "";
            }
        }

        private void setSearchMode()
        {
            SearchMode = !SearchMode;
            if (!SearchMode)
            {
                InputText = "";
            }
        }

        private ObservableCollection<TodoItem> startSearch()
        {
            ObservableCollection<TodoItem> searchTodos = new ObservableCollection<TodoItem>();
            if (InputText == "")
            {
                return _todoHandler.Todos;
            }
            foreach(TodoItem tditm in _todoHandler.Todos)
            {
                if(tditm.Text.Contains(InputText))
                {
                    searchTodos.Add(tditm);
                }
            }
            return searchTodos;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class TodoItem
    {
        public string Id { get; }
        public string Text { get; set; }
        public int Priority { get; set; }
        public bool IsSelected { get; set; }
        public TodoItem(string text, int priority)
        {
            Id = generateID();
            Text = text;
            if (priority > 3) { priority = 3; }
            if (priority < 0) { priority = 0; }
            Priority = priority;
        }

        private string generateID()
        {
            return Guid.NewGuid().ToString("N");
        }
    }

}
