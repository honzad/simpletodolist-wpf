﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleTodoList.Controllers
{
    public static class DataFileHandler
    {
        public static void SaveData(List<TodoItem> serializableObject)
        {
            string folderPath = Path.Combine(Directory.GetCurrentDirectory(), "Data");
            string filePath = Path.Combine(folderPath, "todoData.json");
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }

            File.WriteAllText(filePath, JsonConvert.SerializeObject(serializableObject));
        }

        public static List<TodoItem> LoadData()
        {
            string folderPath = Path.Combine(Directory.GetCurrentDirectory(), "Data");
            string filePath = Path.Combine(folderPath, "todoData.json");
            if (Directory.Exists(folderPath))
            {
                if (File.Exists(filePath))
                {
                    List<TodoItem> ret = JsonConvert.DeserializeObject<List<TodoItem>>(File.ReadAllText(filePath));
                    return ret;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
            
        }
    }
}
