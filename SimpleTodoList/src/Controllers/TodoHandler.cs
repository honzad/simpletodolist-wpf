﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleTodoList.Controllers
{
    public class TodoHandler
    {
        public ObservableCollection<TodoItem> Todos { get; private set; }
        public TodoHandler()
        {
            Todos = new ObservableCollection<TodoItem>();
            this.Load();
        }

        private void Load()
        {
            List<TodoItem> ldata = DataFileHandler.LoadData();
            if (ldata != null)
            {
                if (ldata.Count > 0)
                {
                    Todos = new ObservableCollection<TodoItem>(ldata);
                }
            }
        }

        public void Add(TodoItem item)
        {
            Todos.Add(item);
            DataFileHandler.SaveData(Todos.ToList());
        }

        public bool Remove(string id)
        {
            bool removed = false;
            foreach (TodoItem t in Todos)
            {
                if (String.Equals(id, t.Id))
                {
                    Todos.Remove(t);

                    removed = true;
                    break;
                }
            }

            if (removed)
            {
                DataFileHandler.SaveData(Todos.ToList());
            }

            return removed;
        }
    }
}
